import React from 'react';
import Card from './card';
import NoDataFound from './noData';


class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.postID = 2;
        this.state = {
            cardData: [],
            newDescription: "",
        }
        this.bindMethods();
    }

    bindMethods() {
        this.deleteInfoCard = this.deleteInfoCard.bind(this);
        this.addNewCard = this.addNewCard.bind(this);
        this.newPost = this.newPost.bind(this);
    }

    componentDidMount() {
        this.setState({
            cardData: [
                {
                    cardId: '1',
                    title: 'card1',
                    description: 'lorem text goes here...'
                },
                {
                    cardId: '2',
                    title: 'card2',
                    description: '2 lorem text goes here...'
                },

            ],
        })
    }

    deleteInfoCard(index) {
        let copyCardData = [...this.state.cardData]
        copyCardData.splice(index, 1);
        this.setState({
            cardData: copyCardData
        })
    }

    addNewCard() {

        let copyCardData = [...this.state.cardData];
        copyCardData.push({
            cardId: this.postID + 1,
            title: `card ${this.postID + 1}`,
            description: this.state.newDescription
        })
        this.setState({
            cardData: copyCardData
        })
    }

    newPost(event) {
        this.setState({
            newDescription: event.target.value
        })
    }


    render() {
        return (
            <>
                <div className="container">
                    <div className='col-lg-4'>
                        <label>Description</label>
                        <input type='text' placeholder="Please enter description" class="form-control" onBlur={this.newPost} /><br />
                        <button onClick={this.addNewCard} className='btn btn-primary'> Add Card </button>
                    </div>

                    <br />
                    <br />
                    {this.state.cardData.length > 0 ? this.state.cardData.map((res, index) => {
                        return (
                            <Card
                                key={res.cardId}
                                cardId={res.cardId}
                                title={res.title}
                                description={res.description}
                                deleteCard={() => this.deleteInfoCard(index)}
                            />
                        )
                    }) : <NoDataFound />}


                </div>
            </>
        )
    }
}

export default Dashboard;