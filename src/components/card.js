import React, { useState } from 'react';

// const [setCardData]=useState(0);

const cardData = [
    {
        cardId: '1',
        title: 'card1',
        description: 'lorem text goes here...'
    },
    {
        cardId: '2',
        title: 'card2',
        description: '2 lorem text goes here...'
    },

];


const detailView = (id) => {
    alert(id);
}


const addList = () => {


    cardData.push(
        {
            cardId: '3',
            title: 'card3',
            description: '3 lorem text goes here...'
        },
    )
    alert(cardData.length)
}

const renderAddButton = () => {
    return (
        <>
            <button type="button" onClick={addList} class="btn btn-primary">Add</button>
            <br />
            <br />
        </>
    )
}

const Card = ({ cardId, title, description, deleteCard}) => {
    return (
        <>
            <div class="card mb-2">
                <div class="card-body">
                    <button class="float-right btn btn-secondary" onClick={deleteCard}> Delete </button>
                    <h5 class="card-title">{title}</h5>
                    {description}
                </div>
            </div>
        </>
    )
}

export default Card;