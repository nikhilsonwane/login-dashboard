import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

export default class Login extends React.Component {
    render() {
        const { history } = this.props;
        return (
            <Formik
                initialValues={{
                    email: '',
                    password: '',
                }}
                validationSchema={Yup.object().shape({

                    email: Yup.string()
                        .email('Email is invalid')
                        .required('Email is required'),
                    password: Yup.string()
                        .min(6, 'Password must be at least 6 characters')
                        .required('Password is required'),
                })}
                onSubmit={fields => {
                    if (fields.email=='Clarion@clarion.com' && fields.password=='Clarion123') {
                        // Clarion@clarion.com
                        // Clarion123
                        history.push('/dashboard');
                    } else {
                        alert('Invalid Crendential')
                    }
                }}
                render={({ errors, status, touched }) => (
                    <Form>
                        <div className="container">
                        <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary mr-2">Login</button>
                            </div>
                        </div>
                        </div>
                        </div>
                    </Form>
                )}
            />
        )
    }
}
