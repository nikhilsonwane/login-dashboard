import React from 'react';
import {BrowserRouter,Route, Switch,} from 'react-router-dom';
import Login from './components/login'
import Dashboard from './components/dashboard'

class App extends React.Component {
    render() {
        return (
          <BrowserRouter>
          <Switch>
            <Route exact path='/login' component={Login} />
            <Route exact path='/dashboard' component={Dashboard} />
            <Route path='/' component={Login} />
          </Switch>
          </BrowserRouter>
        )
    }
}

export default App; 